FROM php:7.0-apache

#COPY src/ /var/www/html/

ADD entrypoint.sh /root/entrypoint.sh
ENTRYPOINT ["/root/entrypoint.sh"]
