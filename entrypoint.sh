#!/bin/bash

file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		echo >&2 "error: both $var and $fileVar are set (but are exclusive)"
		exit 1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}

file_env 'URL'
file_env 'LOGIN'
file_env 'PASS'

cat > /var/www/html/fetch-zimbra-calendar.php << EOF
<?php
header('Content-Type: text/calendar');
\$ch = curl_init("${URL}");
curl_setopt(\$ch, CURLOPT_USERPWD, "${LOGIN}:${PASS}");
curl_exec(\$ch);
\$info = curl_getinfo(\$ch);
curl_close(\$ch);
?>
EOF

cd /var/www/html && apache2-foreground
